import {Component, ViewChild} from '@angular/core';
import { Query, DataManager, ODataV4Adaptor } from '@syncfusion/ej2-data';
import {ContextMenuComponent, TreeViewComponent} from '@syncfusion/ej2-angular-navigations';
import { BeforeOpenCloseMenuEventArgs, MenuEventArgs, MenuItemModel } from '@syncfusion/ej2-angular-navigations';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  public localData: any = [
    { id: 1, pid: null, name: 'Steven Buchanan', eimg: '10', job: 'CEO', hasChild: true, expanded: true },
    { id: 2, pid: 1, name: 'Laura Callahan', eimg: '2', job: 'Product Manager', hasChild: true },
    { id: 3, pid: 2, name: 'Andrew Fuller', eimg: '7', job: 'Team Lead', hasChild: true },
    { id: 4, pid: 3, name: 'Anne Dodsworth', eimg: '1', job: 'Developer' },
    { id: 5, pid: 1, name: 'Nancy Davolio', eimg: '4', job: 'Product Manager', hasChild: true },
    { id: 6, pid: 5, name: 'Michael Suyama', eimg: '9', job: 'Team Lead', hasChild: true },
    { id: 7, pid: 6, name: 'Robert King', eimg: '8', job: 'Developer ' },
    { id: 8, pid: 7, name: 'Margaret Peacock', eimg: '6', job: 'Developer' },
    { id: 9, pid: 1, name: 'Janet Leverling', eimg: '3', job: 'HR' },
  ];

  constructor() {
    console.log(this.localData[0].template);
  }

 // @ViewChild('samples')


  public field: object = { dataSource: this.localData, id: 'id', parentID: 'pid', text: 'name', hasChildren: 'hasChild'};


  @ViewChild ('treevalidate', {static: false}) treevalidate: TreeViewComponent;
  @ViewChild ('contentmenutree', {static: false}) contentmenutree: ContextMenuComponent;

  public menuItems: MenuItemModel[] = [
    { text: 'Add New Item' },
    { text: 'Rename Item' },
    { text: 'Remove Item' }
  ];

  public index: number = 1;
  public menuclick(args: MenuEventArgs) {
    const targetNodeId: string = this.treevalidate.selectedNodes[0];
    if (args.item.text === "Add New Item") {
      const nodeId: string = "tree_" + this.index;
      const item: { [key: string]: Object } = { id: nodeId, name: 'New Folder', job: 'CEO'  };
      this.treevalidate.addNodes([item], targetNodeId, null);
      this.index++;
      this.localData.push(item);
      this.treevalidate.beginEdit(nodeId);
    } else if (args.item.text === "Remove Item") {
      this.treevalidate.removeNodes([targetNodeId]);
    } else if (args.item.text === "Rename Item") {
      this.treevalidate.beginEdit(targetNodeId);
    }
  }

  public beforeopen(args: BeforeOpenCloseMenuEventArgs) {
    let targetNodeId: string = this.treevalidate.selectedNodes[0];
    let targetNode: Element = document.querySelector('[data-uid="' + targetNodeId + '"]');
    if (targetNode.classList.contains('remove')) {
      this.contentmenutree.enableItems(['Remove Item'], false);
    } else {
      this.contentmenutree.enableItems(['Remove Item'], true);
    }
    if (targetNode.classList.contains('rename')) {
      this.contentmenutree.enableItems(['Rename Item'], false);
    } else {
      this.contentmenutree.enableItems(['Rename Item'], true);
    }
  }


}
